<?php

    include __DIR__."/../model/database.php";
    $title = "Grade Planning";
    include __DIR__."/../view/header.html.php";
    
    if(isset($_GET['register'])){
        $title = "Register";
        include __DIR__."/../view/register.html.php";
    }else if(isset($_GET['login'])){
        $title = "Login";
        include __DIR__."/../view/login.html.php";
    }else{
        include __DIR__."/../view/home.html.php";
    }